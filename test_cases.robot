*** Settings ***
Documentation     This suite validates the Login Page
Resource    ./resources/login_kw.robot
Suite Setup     Login To System
Suite Teardown    Close Application

*** Variables ***
${user}    testing@sovos.com
${password}   3123123

*** Test Cases ***
Login with Empty Password
    [Tags]  Regression
    Login With Empty Password Should Fail    ${user}

Login with Empty User
  #Test
  Login With Empty User Name Should Fail    ${password}

Login Without Credentials
  Login Without Credentials Should Fail

# Invalid User name
# Invalid Password
# Invalid Password and User Name
