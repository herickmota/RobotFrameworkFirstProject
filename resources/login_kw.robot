*** Settings ***
Resource    ../utils/configs.robot
Resource    ../resources/pageObjects/login.robot

*** Keywords ***
Submit Credentials
  Click Button    ${LOGIN_BTN}

Login With Empty Password Should Fail
  [Arguments]    ${userName}
  Input Text    ${EMAIL_INP}   ${userName}
  Click Button    ${LOGIN_BTN}
  Wait Until Page Contains    The "Password" field is required

Login With Empty User Name Should Fail
  [Arguments]    ${password}
  Input text     ${PASS_INP}  ${password}
  Submit Credentials
  Wait Until Page Contains    The "E-mail" field is required.

Login Without Credentials Should Fail
  Clear Element Text    ${EMAIL_INP}
  Clear Element Text    ${PASS_INP}
  Submit Credentials
  Wait Until Page Contains    The "Password" field is required.
  Wait Until Page Contains    The "E-mail" field is required.


# Empty User Name and Password
#
#
# Invalid User name
#
#
# Invalid Password
#
#
# Invalid Password and User Name
