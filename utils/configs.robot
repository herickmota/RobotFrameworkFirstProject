*** Settings ***
Documentation    This file compiles all the configs required for the test cases.
Library    Selenium2Library

*** Variables ***
${browser}    chrome
${server}    https://qavat.sovos.com

*** Keywords ***
Login To System
  Open Browser    ${server}   ${browser}

Close Application
  Close Browser
